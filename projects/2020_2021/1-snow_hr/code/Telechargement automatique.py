#Il est nécessaire d'avoir les modules suivants : gdal, geopandas, pandas, fiona, shapely, sentinelsat


# connect to the API
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
from datetime import date

api = SentinelAPI("Nom_d'utilisateur", "mot de passe", "https://scihub.copernicus.eu/dhus") #Compte Copernicus Open Access Hub



# search by polygon, time, and SciHub query keywords
footprint = geojson_to_wkt(read_geojson("Chemin du fichier geojson")) #chemin du fichier geojson
products = api.query(footprint,
                     date=('20200101', date(2020, 0o1, 10)),
                     platformname='Sentinel-2',
                     cloudcoverpercentage=(0, 10),limit=2) # Balayage sur 10 jours du mois de Janvier | le nombre de fichier mére est limité à deux afin de minimiser le temps de téléchargement

# download all results from the search
api.download_all(products)

# convert to Pandas DataFrame
products_df = api.to_dataframe(products)

# GeoJSON FeatureCollection containing footprints and metadata of the scenes
api.to_geojson(products)

# GeoPandas GeoDataFrame with the metadata of the scenes and the footprints as geometries
api.to_geodataframe(products)


