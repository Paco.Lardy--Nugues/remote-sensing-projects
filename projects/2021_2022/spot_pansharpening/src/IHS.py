import numpy as np
from utils import upsample_interp23
import cv2
import matplotlib.pyplot as plt
import time


def IHS(pan, hs):

    M, N, c = pan.shape
    m, n, C = hs.shape
    
    ratio = int(np.round(M/m))
        
    print('get sharpening ratio: ', ratio)
    assert int(np.round(M/m)) == int(np.round(N/n))
    
    u_hs = upsample_interp23(hs, ratio)

    I = np.mean(u_hs, axis=-1, keepdims=True)
    
    P = (pan - np.mean(pan))*np.std(I, ddof=1)/np.std(pan, ddof=1)+np.mean(I)
    
    I_IHS = u_hs + np.tile(P-I, (1, 1, 1))

    return np.uint8(I_IHS)


def main():
    start = time.time()

    hs = cv2.imread('../data/1.TIF')
    pan = cv2.imread('../data/1.TIF')

    a = IHS(pan, hs)
    cv2.imwrite('../data/output/mod2.TIF', a)
    # cv2.imwrite('init.TIF', hs)
    # cv2.imwrite('panch.TIF', pan)
    # plt.imshow(pan)
    # plt.show()
    # plt.imshow(hs[:,:,::-1])
    # plt.show()
    plt.imshow(a[:, :, ::-1])
    plt.show()

    end = time.time()

    print(end - start)


if __name__ == "__main__":
    main()
